name: archmage
id: archmage
title: 13th Age
version: "1.29.0"
authors:
  - name: Matt Smith
    discord: asacolips#1867
  - name: Cody Swendrowski
    discord: cswendrowski#9701
  - name: Federico Pederzolli
    discord: Legofed3#6471

description: The official 13th Age system for Foundry Virtual Tabletop.
minimumCoreVersion: "11"
compatibleCoreVersion: "11"

compatibility:
  minimum: "11"
  verified: "11.315"
  maximum: "12"

esmodules:
  - module/archmage.js
  # Vue is included in the actor sheet class. To download new copies, go to
  # https://unpkg.com/browse/vue@3/dist/ and replace `src/scripts/lib/vue.esm-browser.js`
  # and `src/scripts/lib/vue.esm-browser.prod.js`

scripts:
  - scripts/lib/parse-md.js
  - scripts/migrate/archmage-updates.js
  - scripts/lib/intro.js
  # - scripts/vue/archmage-methods.js

styles:
  - css/archmage.css
  - css/v2/archmage-v2.css
  - vue/styles.vue.css
  - css/lib/introjs.css

flags:
  archmage:
    archmage-art: 'systems/archmage/map-archmage.json'
  hotReload:
    extensions:
      - css
      - html
      - hbs
      - json
    paths:
      - css
      - vue
      - templates
      - languages

packFolders:
  - name: Player options
    sorting: m
    packs:
      - pregen-characters
      - generalfeats
      - multiclassfeats
      - races
      - classes
      - barbarian
      - bard
      - cleric
      - fighter
      - paladin
      - ranger
      - animal-companion
      - animal-companions
      - rogue
      - sorcerer
      - wizard
      - chaosmage
      - commander
      - druid
      - monk
      - necromancer
      - necromancer-summons
      - occultist
  - name: Creatures
    sorting: m
    packs:
      - srd-Monsters
      - srd-random-monster-abilities
  - name: Magic Items
    sorting: m
    packs:
      - srd-magic-items-armors
      - srd-magic-items-arrows
      - srd-magic-items-belts
      - srd-magic-items-books
      - srd-magic-items-boots
      - srd-magic-items-chuul-symbiotes
      - srd-magic-items-cloaks
      - srd-magic-items-consumables
      - srd-magic-items-cursed
      - srd-magic-items-gloves
      - srd-magic-items-helmets
      - srd-magic-items-necklaces
      - srd-magic-items-rings
      - srd-magic-items-shields
      - srd-magic-items-staffs
      - srd-magic-items-symbols
      - srd-magic-items-wands
      - srd-magic-items-weapons
      - srd-magic-items-wondrous-items
  - name: Misc
    sorting: m
    packs:
      - conditions
      - system-macros
      - system-rolltables

packs:
  - label: Pregenerated Characters
    type: Actor
    name: pregen-characters
    path: packs/pregen-characters
    system: archmage

  - label: Classes
    type: JournalEntry
    name: classes
    path: packs/classes
    system: archmage

  - label: Conditions
    type: JournalEntry
    name: conditions
    path: packs/conditions
    system: archmage

  - label: Races
    type: Item
    name: races
    path: packs/races
    system: archmage

  - label: Barbarian
    type: Item
    name: barbarian
    path: packs/barbarian
    system: archmage

  - label: Bard
    type: Item
    name: bard
    path: packs/bard
    system: archmage

  - label: Cleric
    type: Item
    name: cleric
    path: packs/cleric
    system: archmage

  - label: Fighter
    type: Item
    name: fighter
    path: packs/fighter
    system: archmage

  - label: Paladin
    type: Item
    name: paladin
    path: packs/paladin
    system: archmage

  - label: Ranger
    type: Item
    name: ranger
    path: packs/ranger
    system: archmage

  - label: Rogue
    type: Item
    name: rogue
    path: packs/rogue
    system: archmage

  - label: Sorcerer
    type: Item
    name: sorcerer
    path: packs/sorcerer
    system: archmage

  - label: Wizard
    type: Item
    name: wizard
    path: packs/wizard
    system: archmage

  - label: Chaos Mage
    type: Item
    name: chaosmage
    path: packs/chaosmage
    system: archmage

  - label: Commander
    type: Item
    name: commander
    path: packs/commander
    system: archmage

  - label: Druid
    type: Item
    name: druid
    path: packs/druid
    system: archmage

  - label: Monk
    type: Item
    name: monk
    path: packs/monk
    system: archmage

  - label: Necromancer
    type: Item
    name: necromancer
    path: packs/necromancer
    system: archmage

  - label: Necromancer (Summons)
    type: Actor
    name: necromancer-summons
    path: packs/necromancer-summons
    system: archmage

  - label: Occultist
    type: Item
    name: occultist
    path: packs/occultist
    system: archmage

  - label: General Feats
    type: Item
    name: generalfeats
    path: packs/general-feats
    system: archmage

  - label: Multiclass Feats
    type: Item
    name: multiclassfeats
    path: packs/multiclass-feats
    system: archmage

  - label: Animal Companion
    type: Item
    name: animal-companion
    path: packs/animal-companion
    system: archmage

  - label: Animal Companions
    type: Actor
    name: animal-companions
    path: packs/animal-companions
    system: archmage

  - label: SRD Monsters
    type: Actor
    name: srd-Monsters
    path: packs/srd-monsters
    system: archmage

  - label: SRD Magic Items - Armors
    type: Item
    name: srd-magic-items-armors
    path: packs/srd-magic-items-armors
    system: archmage

  - label: SRD Magic Items - Arrows
    type: Item
    name: srd-magic-items-arrows
    path: packs/srd-magic-items-arrows
    system: archmage

  - label: SRD Magic Items - Belts
    type: Item
    name: srd-magic-items-belts
    path: packs/srd-magic-items-belts
    system: archmage

  - label: SRD Magic Items - Books
    type: Item
    name: srd-magic-items-books
    path: packs/srd-magic-items-books
    system: archmage

  - label: SRD Magic Items - Boots
    type: Item
    name: srd-magic-items-boots
    path: packs/srd-magic-items-boots
    system: archmage

  - label: SRD Magic Items - Consumables
    type: Item
    name: srd-magic-items-consumables
    path: packs/srd-magic-items-consumables
    system: archmage

  - label: SRD Magic Items - Chuul Symbiotes
    type: Item
    name: srd-magic-items-chuul-symbiotes
    path: packs/srd-magic-items-chuul-symbiotes
    system: archmage

  - label: SRD Magic Items - Cloaks
    type: Item
    name: srd-magic-items-cloaks
    path: packs/srd-magic-items-cloaks
    system: archmage

  - label: SRD Magic Items - Cursed
    type: Item
    name: srd-magic-items-cursed
    path: packs/srd-magic-items-cursed
    system: archmage

  - label: SRD Magic Items - Gloves
    type: Item
    name: srd-magic-items-gloves
    path: packs/srd-magic-items-gloves
    system: archmage

  - label: SRD Magic Items - Helmets
    type: Item
    name: srd-magic-items-helmets
    path: packs/srd-magic-items-helmets
    system: archmage

  - label: SRD Magic Items - Necklaces
    type: Item
    name: srd-magic-items-necklaces
    path: packs/srd-magic-items-necklaces
    system: archmage

  - label: SRD Magic Items - Rings
    type: Item
    name: srd-magic-items-rings
    path: packs/srd-magic-items-rings
    system: archmage

  - label: SRD Magic Items - Shields
    type: Item
    name: srd-magic-items-shields
    path: packs/srd-magic-items-shields
    system: archmage

  - label: SRD Magic Items - Staffs
    type: Item
    name: srd-magic-items-staffs
    path: packs/srd-magic-items-staffs
    system: archmage

  - label: SRD Magic Items - Symbols
    type: Item
    name: srd-magic-items-symbols
    path: packs/srd-magic-items-symbols
    system: archmage

  - label: SRD Magic Items - Wands
    type: Item
    name: srd-magic-items-wands
    path: packs/srd-magic-items-wands
    system: archmage

  - label: SRD Magic Items - Weapons
    type: Item
    name: srd-magic-items-weapons
    path: packs/srd-magic-items-weapons
    system: archmage

  - label: SRD Magic Items - Wondrous Items
    type: Item
    name: srd-magic-items-wondrous-items
    path: packs/srd-magic-items-wondrous-items
    system: archmage

  - label: SRD Random Monster Abilities
    type: Item
    name: srd-random-monster-abilities
    path: packs/srd-random-monster-abilities
    system: archmage

  - label: System Macros
    type: Macro
    name: system-macros
    path: packs/system-macros
    system: archmage

  - label: System RollTables
    type: RollTable
    name: system-rolltables
    path: packs/system-rolltables
    system: archmage

languages:
  - lang: en
    name: English
    path: languages/en.json

  - lang: es
    name: Español
    path: languages/es.json

socket: true
url: https://gitlab.com/asacolips-projects/foundry-mods/archmage
manifest: https://gitlab.com/asacolips-projects/foundry-mods/archmage/-/raw/master/system.json
download: https://gitlab.com/asacolips-projects/foundry-mods/archmage/-/jobs/artifacts/1.29.0/raw/archmage.zip?job=build-prod
changelog: https://gitlab.com/asacolips-projects/foundry-mods/archmage/-/blob/master/CHANGELOG.md
initiative: 1d20 + (@abilities?.dex.mod || 0) + @attributes.init.value + @attributes.level.value
gridDistance: "1"
gridUnits: ''
primaryTokenAttribute: attributes.hp

# Manifest+ Props
manifestPlusVersion: "1.1.0"
media:
  - type: cover
    url: https://mattsmithin-files.s3.amazonaws.com/toolkit13.png
